


  var app = angular.module('MineSweeper', []); 
	gameStarted = true;
	
	app.factory('myService', function() {
		 var savedData = {}
		 function set(rows, mines) {
		   savedData = {rows,mines};
		 }
		 function get() {
		  return savedData;
		 }

		 return {
		  set: set,
		  get: get
		 }

	});
	
	
	/**** is there some more elegant way to do it?***/
	app.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
	});
	


	app.controller('ScreenController',['$scope','myService', function ($scope, myService) {
	myService.set(10,5);
	this.gameStarted = false;
	this.mines = 10;
	this.rows = 5;
	
	$scope.startGame = function(screen) {
		var r = screen.rows;
		if (screen.mines > Math.floor(r*r/2)){
			alert("can't put more than " + Math.floor(r*r/2) + " mines for this board dimensions!");
			return;
		}
		myService.set(screen.rows, screen.mines);			
		screen.gameStarted = true;
	};
 }]);

  
  app.controller('GameController', function($scope, myService){	
	//alert(myService.get());
	$scope.Math = window.Math;
	this.MINES = myService.get().mines;
	this.ROWS = myService.get().rows;
	
	//========================================================
	/*Returns single array with length rows*cols that has '1' on places with mines*/
	function spawnMines (mines, rows, cols){
		var array = new Array(rows*cols);
		for (var i=0;i<rows*cols;i++)
			array[i] = i;
			
		var places = new Array(mines);// array that hold mines places only
		
		for (var i=0; i<mines; i++){
			var number = Math.floor((Math.random() * (rows*cols-i))+ i);//there are rows*cols cell - spawn numbers in this range: 0...rows*cols
			places[i] = array[number];
			
			var temp = array[i];
			array[i] = array[number];
			array[number] = temp;
		}
		
		for (var i=0;i<rows*cols;i++)		
			array[i] = 0;
		for (var i=0;i<mines;i++)		
			array[places[i]] = 1;
		return array;
	
	
	}
	
	//===========================================================
	function checkNeighbours(game, row,col){
		for (var m = row-1; m<=row+1; m++){
			for (var n = col-1; n<=col+1; n++){
				if (m != row || n != col){ //not the pressed cell
				 if (m>=0 && m<game.ROWS && n>=0 && n<game.ROWS){					
					 if (game.board.rows[m].cols[n].value == 0 && !game.board.rows[m].cols[n].pressed){//empty non-pressed cell - check around too
						 game.board.rows[m].cols[n].pressed = true;
						 checkNeighbours(game, m,n);//recursively check surroundings
						 game.board.unpressed--;	
					 }
					 else if (!game.board.rows[m].cols[n].pressed){//non empty cell - just press it
							game.board.rows[m].cols[n].pressed = true;//simply reveal the content ,later on we check if it's a mine
							game.board.unpressed--;	
						 }						 
				 }
				}
			}						
		}		
	}
	
	//=============================================================
	this.pressCB = function(game){		

		this.endGame = function(game, msg){
			for (var i=0; i<game.ROWS ; i++){			
				for (var j=0; j< game.ROWS; j++){				
					game.board.rows[i].cols[j].pressed = true;
				}
			}
			
			$scope.$apply();//refresh board after its reveal						
			res = confirm(msg);
			if (res == true){//Start a new game
				game.startNewGame();
			}
			else {// end game - go back to main screen
				location.href = '\MainScreen.html';						
			}
			

		}
		
		
		if (this.pressed)
			return;
		
		game.board.unpressed--;			
		this.pressed = true;
		
		if (this.mine == true){	 //Stepped on a mine - end game
			this.endGame(game, "YOU LOSE!!! Would you like to start a new game?");				
		}
		else{
			if (this.value == 0){// no mine in there -recursively show the surroundings
				checkNeighbours(game,this.rowNum, this.colNum);				
			}
			if (game.board.unpressed == game.MINES){	 //only the real mines were not pressed yet - the player won
					this.endGame(game, "YOU WIN!!! Would you like to start a new game?");							
			}
		}
		
	}
	
	//=============================================================
	this.rightClickCB = function(game){
		this.marked = !this.marked; //toggle the mine mark status
		if (this.marked){
			if (game.board.minesMarked< game.MINES){ //can't mark more mines than there actually are
				game.board.minesMarked++;
			}
			else{
				this.marked = false;
			}
		}
		else
			game.board.minesMarked--;		
	}
	
	//=============================================================
    this.createBoard = function () {	 		
		minesPlaces = spawnMines(this.MINES, this.ROWS , this.ROWS);
		
		//initialize all cells
		var Board={};
		Board.rows=[]; //need to initialize rows like this so we can use push function
		var k=0;
		for (var i=0; i<this.ROWS ; i++){
			var row={}
			row.cols=[];
			
			for (var j=0; j< this.ROWS; j++){
				/**Cell initialization**/
				var col={value:0, pressed:false, marked:false, handlePress:this.pressCB, 
						markMine:this.rightClickCB , rowNum:i, colNum:j };			
				
				if (minesPlaces[k] == 1)//set a mine in this cell
					col.mine= true;					
				else
					col.mine=false;				
				
				k++;
				row.cols.push(col);//insert this cell to columns array
			}
			
			Board.rows.push(row);		
		}
		
		//count number of mines nearby
		for (var i=0; i<this.ROWS ; i++){			
			for (var j=0; j< this.ROWS; j++){				
				if (Board.rows[i].cols[j].mine == true){
					for (var m = i-1; m<=i+1; m++){
						for (var n = j-1; n<=j+1; n++){
							if (m != i || n !=j){ //not the mine cell
							 if (m>=0 && m<this.ROWS && n>=0 && n<this.ROWS)
								 Board.rows[m].cols[n].value++;
							}
						}						
					}					
				}
				
			}			
		}
		Board.unpressed = this.ROWS*this.ROWS; //how much cells were not revealed
		Board.minesMarked=0;
		return Board;
	   
	}
	
	
	this.startNewGame = function(){
		this.minesMarked = 0;
		this.board = this.createBoard(); 
		this.mines=[];
		for (var i=0; i<this.ROWS ; i++){
			for (var j=0; j< this.ROWS; j++){
				if (this.board.rows[i].cols[j].mine == true)
					this.mines.push(i*this.ROWS +(j+1));
			}
		}
	};
	this.startNewGame();
	//
});
 


