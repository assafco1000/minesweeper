


  var app = angular.module('MineSweeper', []); 
	gameStarted = true;
	
	//=========================================================
	app.factory('myService', function() {
		 var savedData = {}
		 
		 //========================
		 function set(rows, mines) {
		   savedData = {rows,mines};
		 }
		 
		 //========================
		 function get() {
		  return savedData;
		 }
		 
		 //========================
		 /*Returns single array with length rows*cols that has '1' on places with mines*/
		 /*Private method*/
		function spawnMines (mines, rows){
			var array = new Array(rows*rows);
			for (var i=0;i<rows*rows;i++)
				array[i] = i;
				
			var places = new Array(mines);// array that hold mines places only
			
			for (var i=0; i<mines; i++){
				var number = Math.floor((Math.random() * (rows*rows-i))+ i);//there are rows*cols cell - spawn numbers in this range: 0...rows*cols
				places[i] = array[number];
				
				var temp = array[i];
				array[i] = array[number];
				array[number] = temp;
			}
			
			for (var i=0;i<rows*rows;i++)		
				array[i] = 0;
			for (var i=0;i<mines;i++)		
				array[places[i]] = 1;
			return array;
		
		
		}
		
		function createBoard() {	 		
		minesPlaces = spawnMines(savedData.mines, savedData.rows);
		
		//initialize all cells
		var Board={};
		Board.rows=[]; //need to initialize rows like this so we can use push function
		var k=0;
		for (var i=0; i<savedData.rows ; i++){
			var row={}
			row.cols=[];
			
			for (var j=0; j< savedData.rows; j++){
				////Cell initialization//
				var col={value:0, pressed:false, marked:false, rowNum:i, colNum:j };			
				
				if (minesPlaces[k] == 1)//set a mine in this cell
					col.mine= true;					
				else
					col.mine=false;				
				
				k++;
				row.cols.push(col);//insert this cell to columns array
			}
			
			Board.rows.push(row);		
		}
		
		//count number of mines nearby
		for (var i=0; i<savedData.rows ; i++){			
			for (var j=0; j< savedData.rows; j++){				
				if (Board.rows[i].cols[j].mine == true){
					for (var m = i-1; m<=i+1; m++){
						for (var n = j-1; n<=j+1; n++){
							if (m != i || n !=j){ //not the mine cell
							 if (m>=0 && m<savedData.rows && n>=0 && n<savedData.rows)
								 if (Board.rows[m].cols[n].mine == false)
									Board.rows[m].cols[n].value++;
							}
						}						
					}					
				}
				
			}			
		}
		
			Board.unpressed = savedData.rows*savedData.rows; //how much cells were not revealed
			Board.minesMarked=0;
			return Board;	   
		}
	
	    //========================

		 return {
		  set: set,
		  get: get,
		  createBoard:createBoard
		 }
	});
	
	
	/**** is there some more elegant way to do it?***/
	app.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
	});
	

	//=========================================================
	app.controller('ScreenController',['$scope','myService', function ($scope, myService) {
	myService.set(10,5);
	this.gameStarted = false;
	this.mines = 10;
	this.rows = 5;
	
	$scope.startGame = function(screen) {
		var r = screen.rows;
		if (screen.mines > Math.floor(r*r/2)){
			alert("can't put more than " + Math.floor(r*r/2) + " mines for this board dimensions!");
			return;
		}
		myService.set(screen.rows, screen.mines);			
		screen.gameStarted = true;
	};
 }]);

  //=========================================================
  app.controller('GameController', function($scope, myService){	
	//alert(myService.get());
	$scope.Math = window.Math;
	this.MINES = myService.get().mines;
	this.ROWS = myService.get().rows;
	
	//===========================================================
	function checkNeighbours(game, row,col){
		for (var m = row-1; m<=row+1; m++){
			for (var n = col-1; n<=col+1; n++){
				if (m != row || n != col){ //not the pressed cell
				 if (m>=0 && m<game.ROWS && n>=0 && n<game.ROWS){					
					 if (game.board.rows[m].cols[n].value == 0 && !game.board.rows[m].cols[n].pressed){//empty non-pressed cell - check around too
						 game.board.rows[m].cols[n].pressed = true;
						 checkNeighbours(game, m,n);//recursively check surroundings
						 game.board.unpressed--;	
					 }
					 else if (!game.board.rows[m].cols[n].pressed){//non empty cell - just press it
							game.board.rows[m].cols[n].pressed = true;//simply reveal the content ,later on we check if it's a mine
							game.board.unpressed--;	
						 }						 
				 }
				}
			}						
		}		
	}
	
	//=============================================================
	this.pressCB = function(row, col){		

		this.endGame = function(msg){
			for (var i=0; i<this.ROWS ; i++){			
				for (var j=0; j< this.ROWS; j++){				
					this.board.rows[i].cols[j].pressed = true;
					$scope.$apply();
				}
			}
			
			//$scope.$apply();//refresh board after its reveal		
			res = $scope.$apply(confirm(msg));
			if (res == true){//Start a new game
				this.startNewGame();
			}
			else {// end game - go back to main screen
				location.href = '\MainScreen.html';						
			}
		}		
		
		if (this.board.rows[row].cols[col].pressed)
			return;
		
		this.board.unpressed--;			
		this.board.rows[row].cols[col].pressed = true;
		
		if (this.board.rows[row].cols[col].mine == true){	 //Stepped on a mine - end game
			this.endGame("YOU LOSE!!! Would you like to start a new game?");				
		}
		else{
			if (this.board.rows[row].cols[col].value == 0){// no mine in there -recursively show the surroundings
				checkNeighbours(this,row, col);				
			}
			if (this.board.unpressed == this.MINES){	 //only the real mines were not pressed yet - the player won
					this.endGame("YOU WIN!!! Would you like to start a new game?");							
			}
		}
		
	}
	
	//=============================================================
	this.rightClickCB = function(row, col){		
		this.board.rows[row].cols[col].marked = !this.board.rows[row].cols[col].marked; //toggle the mine mark status
		if (this.board.rows[row].cols[col].marked){
			if (this.board.minesMarked< this.MINES){ //can't mark more mines than there actually are
				this.board.minesMarked++;
			}
			else{
				this.board.rows[row].cols[col].marked = false;
			}
		}
		else
			this.board.minesMarked--;		
	}
	
	
	//=============================================================
	this.startNewGame = function(){
			this.minesMarked = 0;
			this.board = myService.createBoard(this.pressCB, this.rightClickCB); 
			
			//FOR DEBUG PURPOSE ONLY
			this.mines=[];
			for (var i=0; i<this.ROWS ; i++){
				for (var j=0; j< this.ROWS; j++){
					if (this.board.rows[i].cols[j].mine == true)
						this.mines.push(i*this.ROWS +(j+1));
				}
			}
		}
		
	this.startNewGame();//start the game
	
});
 


